<?php

Route::group(['middleware' => 'web', 'prefix' => 'followers', 'namespace' => 'Modules\Followers\Http\Controllers'], function()
{
    Route::post('/follow/{id}', 'FollowersController@follow')->name('followers.follow');
    Route::post('/load', 'FollowersController@load')->name('followers.load');
    Route::post('/search', 'FollowersController@search')->name('followers.search');
});
