<?php

namespace Modules\Followers\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Modules\Followers\Entities\Follower;
use Modules\Users\Models\User;

class FollowersController extends Controller
{

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function follow(Request $request)
    {
        try {
            if (!\Auth::check()) {
                throw new \InvalidArgumentException(__('User not authorized'));
            }

            if (!$request->id || !$request->type) {
                throw new \InvalidArgumentException(__('Parameters not correctly passed'));
            }

            $className = $request->type;
            if (!class_exists($className)) {
                throw new \RuntimeException(__('Class name not define'));
            }

            $model = new $className();

            /** @var \Modules\Followers\Entities\Followable $entity */
            $entity = $model->find($request->id);
            if (!$entity) {
                throw new \RuntimeException(__('Failed to get object of specified type'));
            }

            $isAdd = $entity->toFollow();

            $entity = $model->with('followers')->find($request->id);

            return response()->json([
                'status' => 'true',
                'add' => $isAdd,
                'followers' => $model->getFollowers($entity)
            ]);
        } catch (\Exception $e) {
            $json = [
                'success' => false,
                'error' => [
                    'code'    => $e->getCode(),
                    'message' => $e->getMessage(),
                    'trace'   => $e->getTraceAsString()
                ],
            ];

            return response()->json($json, 500);
        }
    }

    public function load(Request $request)
    {
        try {
            if (!$request->type || !$request->items) {
                throw new \InvalidArgumentException(__('Parameters not correctly passed'));
            }

            $className = $request->type;
            if (!class_exists($className)) {
                throw new \RuntimeException(__('Class name not defined'));
            }

            $items = json_decode($request->items);
            $entity = collect([]);

            $followers = [];

            if ($items) {
                $model = new $className();
                $entites = $model->with('followers')->find($items);
                if (!$entity) {
                    throw new \RuntimeException(__('Failed to get object of specified type'));
                }

                $followers = $model->getFollowersFromCollection($entites);
            }

            return response()->json([
                'status' => 'true',
                'data' => $followers
            ]);
        } catch (\Exception $e) {
            $json = [
                'success' => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString()
                ],
            ];

            return response()->json($json, 500);
        }
    }

    public function search(Request $request)
    {
        $me = auth()->user();
        $terms = $request->get('query');
        $limit = config('followers.limit');
        $mysqlFullName = <<<WHERE
            CONCAT_WS(
                ' ',
                `name`,
                `middle_name`,
                `last_name`,
                `nickname`
            )
WHERE;

        $followers = Follower::select('*', User::getDBFullName())->where('followable_id', $me->id)
                        ->where('followable_type', 'Modules\Users\Models\User')
                        ->join('users', function ($query) use ($terms, $mysqlFullName) {
                            $query->on('users.id', '=', 'user_id')
                                ->where(DB::raw($mysqlFullName), 'like', "%$terms%")
                                ->where('is_active', 1);
                        });

        if ($limit) {
            $followers->limit($limit);
        }
        $followers = $followers->get();
        return response(User::getResponseData($followers)->toJson());
    }

}
