$(function () {
    $('.js-selected-recommendation-type').on('click', function () {
        let $input = $(this).find('input:checked');
        if ($input.data('id') === 'selected_followers') {
            $('.select-followers').removeClass('d-none');
        } else {
            $('.select-followers').addClass('d-none');
        }
    });
});