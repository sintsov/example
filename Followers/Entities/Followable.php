<?php
/**
 * Class description
 *
 * @author Sintsov Roman <romiras_spb@mail.ru>
 * @copyright Copyright (c) 2018, coolbeez-recommended-service
 */
declare(strict_types=1);

namespace Modules\Followers\Entities;

use Illuminate\Support\Collection;
use Modules\Users\Models\User;

/**
 * Trait Followable
 * @package Modules\Followers\Entities
 */
trait Followable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function followers()
    {
        return $this->morphMany(Follower::class, 'followable');
    }

    public function toFollow($userId = null): bool
    {
        $userId = $this->getUserId($userId);
        $model = $this->followers();
        $record = $model->getByUser($userId)->first();
        if (!$record) {
            $model->create(['user_id' => $userId]);
            return true;
        } else {
            $model->delete();
            return false;
        }
    }

    /**
     * Get current user id or get user id passed in.
     *
     * @param int|null $userId
     * @return int
     */
    public function getUserId($userId = null): int
    {
        if (is_null($userId)) {
            $userId = auth()->id();
        }
        if (!$userId) {
            throw new \LogicException('User not defined');
        }
        return $userId;
    }

    public function getFollowersFromCollection($entites)
    {
        $users = collect([]);
        $usersWithFollowers = [];

        foreach ($entites as $entity) {
            foreach ($entity->followers as $item) {
                $usersWithFollowers[$entity->id][] = $item->user_id;
                $users->push($item->user_id);
            }
        }

        $followers = $this->getFollowersData($users);

        foreach ($usersWithFollowers as $userId => $items) {
            foreach ($items as $key => $value) {
                $usersWithFollowers[$userId][$key] = $followers[$value];
            }
        }

        return $usersWithFollowers;
    }

    public function getFollowers($entity)
    {
        $users = collect([]);

        foreach ($entity->followers as $item) {
            $users->push($item->user_id);
        }

        return $this->getFollowersData($users);
    }

    protected function getFollowersData($users): Collection
    {
        $followersList = (new User())
            ->select([
                'id',
                'name',
                'last_name',
                'middle_name',
                'is_active',
                'avatar',
                'nickname'
            ])
            ->onlyActive()
            ->find($users->unique())
            ->keyBy('id');

        return $this->getResponseData($followersList);
    }

    public static function getResponseData($followersList): Collection
    {
        return $followersList->map(function ($user) {
            $user->avatar = asset("storage/{$user->avatar}/crop/40x40");
            return $user->only(['id', 'full_name', 'avatar', 'nickname']);
        });
    }
}
