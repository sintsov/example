<?php
/**
 * Class description
 *
 * @author Sintsov Roman <romiras_spb@mail.ru>
 * @copyright Copyright (c) 2019, coolbeez-recommended-service
 */
declare(strict_types=1);

namespace Modules\Followers\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Users\Models\User;

/**
 * Class Follower
 * @package Modules\Followers\Entities
 */
class Follower extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id'];

    /**
     * Followable model relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function followable()
    {
        return $this->morphTo();
    }

    public function scopeGetByUser($query, $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
